.PHONY: test lint build

test:
	composer install
	XDEBUG_MODE=coverage vendor/bin/phpunit

lint:
	composer install
	vendor/bin/phpstan analyze --level max src

build:
	composer install --no-dev
