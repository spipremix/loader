# SpipRemix Loader

Un `spip_loader` revampé.

Basé sur `symfony/http-client`, ce script s'appuie sur la
[v6.0](https://symfony.com/doc/current/http_client.html) de la librairie et
nécessite donc [PHP 8.0.2](https://www.php.net/releases/8.0/fr.php) ou plus.

Il exige les extensions suivantes :

- [zip](https://www.php.net/manual/fr/zip.setup.php) et
- [mbstring](https://www.php.net/manual/fr/mbstring.installation.php).

De plus, l'extension [curl](https://www.php.net/manual/fr/book.curl.php) est recommandée.
Enfin, la directive [memory_limit](https://www.php.net/manual/fr/ini.core.php#ini.memory-limit)
doit est fixée au minimum à 32M pour que le script foncionne sans planter.

S'appuie sur `symfony/finder` OU `league/flysystem` et `symfony/console`.

Le choix est fait, dans un premier temps, de ne pas utiliser
`symfony/dependency-injection`.
À la place, un objet [Context](src/Context.php) receuille les services,
les paramètres et les éléments caclulés pendant le processus d'exécution.

Le choix est fait de ne pas utiliser `league/pipeline`.

TODO : Fournir un phar, outil standalone pour la ligne de commande, un autre phar compatible web pour être utilisé comme son prédécésseur.

En tant que librairie, ce composant pourrait être intégré à SPIP lui-même pour remplacer la fonctionalité de mise à jour native, une éventuelle commande terminal, rendant ainsi le plugin archiviste obsolète ainsi que la fonction `inc/distant(...)` pour ce type d'opération.

TODO : configuration d'un `channel` pour accèder au pre-releases et à la version de développement.

TODO : `self-update` des PHARs si la version de PHP le permet.

TODO : auto-check de la version de PHP pour déterminer si le loader est compatible.

TODO : `symfony/translation`, `twig/twig`, implémentation de `psr/log` et a priori pas `monolog/monolog`

TODO : [htmx](https://htmx.org/] pour la WebUI

## Variables d'environnement

- `SPIP_LOADER_CHANNEL`
- `SPIP_LOADER_UPDATE_LEVEL`
- `SPIP_API_URL`
- `SPIP_INSTANCE_DIRECTORY`

- `SPIP_CACHE_DIRECTORY`

- `SPIP_FAKE_FREE_SPACE`
- `SPIP_FAKE_PLATFORM_VERSION`
- `SPIP_FAKE_PLATFORM_MEMORY_LIMIT`
- `SPIP_FAKE_PLATFORM_EXTENSIONS`

## Éléments de contexte

### Service

- `http_client`

### Paramètres

- `channel`
- `update_level`
- `force`

- `instance`
- `platform`
- `cache`
- `api`

- `cleanup_cache`

- `branch_version`
- `extensions_version`
- `url_version`
- `sha1`
- `zip_size`

## Étapes

- Init
- Platform
- Network
- Instance
- Cache
- VersionSelector
- "Interactivité" (pseudo étape géré dans la commande, voire dans un formulaire web)
- Download
- Compare
- Extract
- CleanupInstance
- CleanupCache
- PostInstall

## Instance

- `directory`
- `freespace`
- `empty`
- `version`

- `filesToDelete`
- `size`

## Cache

- `directory`
- `freespace`
- `cleanable`

## Plate-forme

- `version`
- `memory_l_imit`
- `extensions`

## Usage

- `spipremix_loader -V`
- `spipremix_loader -h`
- `spipremix_loader [-c|--channel CHANNEL] [-u|--update-level UPDATE-LEVEL] [-f|--force] [--] [<instance> [<version>]]`

## Processus

0. Préambule

    Comme son prédécesseur, il n'assure pas de rollback automatisé.
    Les principes de mise à jour n'ayant jamais fait l'objet d'évolutions
    permettant de revenir en arrière après une mise à jour de schéma de BDD.
    Avec ce Loader comme avec son prédecesseur, la mise à jour d'une instance SPIP ne se fait qu'en allant de l'avant.
    Le retour en arrière n'est pas prévu. Et ceci est valable, quelque soit la méthode utilisée (git, checkout).
    De même, et pour les mêmes raisons, le backup avant opérer, n'est pas prévu.
    Enfin, il délègue à SPIP lui-même la mise à jour du schéma de BDD après avoir effectué sa tâche.

1. L'instance SPIP

    Le loader va installer ou mettre à jour une instance SPIP: C'est-à-dire **un réptertoire du filesystem local** dans lequel on a soit :

    - rien : c'est un répertoire vide, et on va faire une installation,
    - un spip : un répertoire dans lequel il y a déjà un spip, et on va faire une mise à jour,
    - autre chose : un répertoire non-vide dans lequel spip n'est pas installé, et on va interrompre le processus.

    On SAIT que SPIP est installé si le fichier `ecrire/inc_version.php` est présent (`is_readable()`) dans le répertoire cible.
    La présence de ce fichier permet alors de déduire la version de SPIP installée.
    Pour SPIP<=4.1 :`grep '$spip_version_branche = ' ecrire/inc_version.php | tr '"' "'" | cut -d\' -f2 | cut -d. -f1,2` => X.Y

    Ce répertoire DOIT être accessible en écriture, récursivement dans le cas d'une mise à jour. C'est le premier pré-requis.

2. Les versions SPIP disponibles

    Les pré-requis supplémentaires suivants DOIVENT être vérifier avant de procéder à l'installation ou la mise à jour :

    - Il DOIT être possible de télécharger une version de SPIP.
    - Une version est un fichier ZIP exposé sur **un site web sans authentification** via la méthode HTTP GET qui doit donc être joignable.
    - L'ensemble des versions téléchargeables est fournie par une API Web sur **un autre site web sans authentification** via la méthode HTTP GET qui doit donc, lui aussi, être joignable.

3. Les capacités de la plate-forme d'accueil de de l'instance

    - La plate-forme qui accueille l'instance SPIP DOIT fournir **une version de PHP** compatible avec la version de SPIP désirée.
    - Cette plate-forme DOIT fournir **les extensions requises** par la version de SPIP désirée.
    - Cette plate-forme DOIT fournir **la memory_limit minimum requise** par la version de SPIP désirée.
    - Cette plate-forme DOIT fournir **un minimum d'espace disque requis** par la version de SPIP désirée.
    - Le processus d'installation lui-même PEUT nécessiciter **un minimum d'espace disque temporaire**.
      - la taille du zip à télécharger peut être connue via un HEAD ()
    - L'instance SPIP DOIT disposer de **l'espace disque** nécessaire à son bon fonctionnement.

    On doit donc :

    - permettre à l'utilisateur de choisir une version SPIP compatible avec les capacités parmis les version SPIP disponibles (celles que l'API propose)
    - ou déterminer quelle est la version la plus appropriée. Pour toute version SPIP disponible, vérifier les pré-requis et sélectionner la  meilleure version possible.

    Pour connaitre les pré-requis d'une version SPIP, l'API Web doit les fournir  :

    - les versions PHP sont des chaines de carartères : `['7.4', '8.0', 8.1']`
    - la RAM, l'espace disque sont des entiers en octets,
    - les extensions sont une combinaison logique pour SPIP<=4.1: `(xml:* ET gd:2 ET (mysqli:* OU (pdo:* ET pdo_sqlite:*))`
    - le loader requiert l'extension `zip:*`, l'extension `curl:*` est recommandée.

4. L'étape suivante consiste à télécharger le fichier ZIP de la version SPIP désirée puis :

    - Pour une installation, décompresser le contenu du ZIP dans le répertoire cible,
    - Pour une mise à jour :
      - comparer les fichiers présents avec les fichiers du ZIP afin de déterminer les fichiers devant être supprimés,
      - décompresser le contenu du ZIP dans le répertoire cible,
      - supprimer les fichiers qui doivent l'être.

    Les fichiers de personnalisation présents dans le répertoire cible ne doivent pas être pris en compte pour la suppression :

    - `config/connect.php`
    - `config/chmod.php`
    - `config/mes_options.php`
    - `squelettes/`
    - `plugins/`
    - `.htaccess`
    - `tmp/`
    - `local/`
    - `IMG/`
    - les cas particuliers (`.git/`, `lib/`, les bases sqlite `config/bases`, la mutualisation, les trucs exotiques type autres fichiers dans config/ que ecran_securite.php, `embed/` dans seenthis, ...) ...
    - le(s) fichier(s) du loader lui-même, si, par exemple, il est à la racine du répertoire cible !

    En gros, la liste des fichiers potentiellement supprimables devrait se limiter à ceux présents dans les répertoires de l'instance et livrés avec le ZIP suivants :

    - `config/ecran_securite.php`
    - `ecrire/**/*`
    - `plugins-dist/**/*`
    - `prive/**/*`
    - `squelettes-dist/**/*`
    - `.gitignore`
    - `CHANGELOG.txt`
    - `COPYING.txt`
    - `INSTALL.txt`
    - `SECURITY.md`
    - `htaccess.txt`
    - `index.php`
    - `plugins-dist.json`
    - `spip.php`
    - `spip.png`
    - `spip.svg`

    On dresse la liste des fichiers qui matchent avec cette liste dans l'instance et le ZIP.
    On fait un diff des deux listes, les fichiers supprimables sont ceux présents sur l'instance et absents du ZIP.

    De fait, dans le cas d'une installation, puisqu'il n'y a pas de fichiers dans l'instance, il n'y aura pas de fichiers à supprimer.
    Une installation est donc équivalente à une mise à jour sans suppression de fichiers.
    Il n'y a pas de distinction à faire entre installation et mise à jour.

    En plus des fichiers distribués, les répertoires `['IMG', 'local', 'tmp']` doivent être créés lors d'une primo-installation.

5. Nettoyage

    Si le processus d'installation a généré des fichiers temporaires, il faut les supprimer.

6. La dernière étape consiste à :

    - indiquer par un message que la suite de l'installation se fait en se rendant à l'URL `ecrire/?exec=install`
    - rediriger vers l'URL `ecrire/?exec=install`
    - poursuivre l'installation/mise à jour par un autre moyen s'il existe. Genre, mettre à jour des plugins non-dist, mettre à jour le schema de la base de données, assurer que certains dossiers seront accesssibles en écriture en phase d'exécution de l'instance.

## SOLIDité

Pour vérifier les pré-requis, le loader s'appuie sur 3 notions :

- le schema, qui détermine quels critères doivent être vérifiés (`requirements`),
- le contexte, qui détermine quelles sont les capacités de la plateforme et de l'instance visé (`capabilities`),
- le mécanisme de comparaison entre les critères et les capacités (`Checker`).
- Ce répertoire DOIT être accessible en écriture, récursivement dans le cas d'une mise à jour

```php
$dirs = [];
if ($context->get('spip.version'))  {
    // @todo check récursif de droit en écriture des répertoires à installer
    // la liste des fichiers supprimables moins les fichiers racines moins les répertoires traités "par un autre moyen s'il existe"
    $dirs = ['config', 'ecrire', 'plugins-dist', 'prive', 'squelettes-dist'];
}
is_writeable('/path/to/spip/instance');
// $context->set('spip.writeable', true);
```

## Compilation

```bash
rm -f version.php
V=$(git describe --tags --abbrev=0 2>/dev/null)
if test -z $V; then echo "KO (no tag found)";false;else echo "<?php return '$V';" > version.php;fi
if test $? -eq 0; then php -d phar.readonly=0 bin/compile;fi
```

bin/compile intègre les répertoires suivants :

Pour le cli :

- src (sauf src/Controller)
- bin
- vendor (moyenant quelques exclusions comme twig si possible)

Pour le web :

- src (sauf src/Command)
- public
- vendor (moyenant quelques exclusions comme console si possible)

## Package

en tant que package composer, intégré à SPIP, il permet la mise à jour du site sans la présence de l'outil compilé
