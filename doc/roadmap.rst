Roadmap
=======

Objet Context : gérer les paramètres, service et calcul au fil du traitement
Objets Item :
    Instance : gérer le répertoire de destination de l'opération (et éventuellement le serveur de destination)
    Cache : espace disque où télécharger une archive (zip)
    Platform : informations concernant l'état de PHP sur le serveur de destination de l'opération
    Api  et Versions : données concernants les archives (zip) et leurs besoins
Objet Pipeline : traiter par étape l'installation, la mise à jour ou la mise à niveau de l'application
Objet Stage :
    Init (paramètres et besoins du loader)
    Platform (version, memory_limit, extensions)
    Network (http request, api)
    Instance (directory, version installée, espace disque, réduire le risque d'installation sur fausse instance)
    Cache (directory, espace disque)
    Selection (détermine les versions installables)
    Interaction (choisir si installables>1, si non-interactif, auto-selectionne la première)
    Download (télécharger le zip)
    Compare (pour  suppression de fichiers obsolètes)
    Extract (dézippe, création répertoires applicatifs non-distribués)
    CleanupInstance (suppression de fichiers obsolètes)
    CleanupCache (suppression de cache temporaire)
    CleanupLoader (suppression du loader temporaire)
    PostInstall (v6 redirection vers suite de l'installation applicative, v7 envoi d'infos à l'API pouir statistiques anonymisées)
Objet Loader : mirco kernel, application de base de l'outil
Objet Filesystem : helper de gestion de fichiers (v6 local, v7 remote via stream ftp/ssh)
Objet Command : implémentation de l'outil en  mode cli
Objet Controller : implémentation de l'outil en mode web
Bootstrap cli
Bootstrap web
Compilateur (pour les modes cli et web)
PHPDoc
Tests Unitaires
phpstan
php-cs-fixer
Tests Fonctionnels

Questions sur la compat php de la v6/v7
---------------------------------------

Voir [13 janvier 2022](https://docs.google.com/spreadsheets/d/1o_XPCk7SFsZNBAmaGcO-IC1swArUQXjJ-2zHkzaJcw4/edit#gid=816058470)

La version PHP médiane est PHP 5.4
La version SPIP médiane est SPIP 3.1

autrement dit, la moitié des sites publics visités par univers_spip sont en PHP5.4 ou moins, et en SPIP 3.1 ou moins

        sf  php min php max
symfony 2.8 5.3.9   5.6
symfony 3.4 5.5.9   7.4?
symfony 4.4 7.1.3   ?
symfony 5.4 7.2.5   ?
symfony 6.x 8.0.2   ?

80% des sites connus en SPIP tournent avec PHP<=5.6 (27% 5.6, 54% <5.6)

répartitions des versions spip au 10/01

1.9-2.1 3.0-3.1 3.2  ("3.3")4.0+ total
1130    1662    2097 250         5139
22%     32%     41%  5%

répartitions des versions php au 10/01
<5.6 5.6   7.0-7.4 8.0+ total
1432 707   499     10   2648 (51%)
54%  26.5% 19%     0.5%

la majorité des sites 3.0 et 3.1 tournent en 5.6
moins de 8% des 3.2 tournent avec PHP<5.6

Il faudrait une version du loader en 5.6 qui permet l'update en 3.2 (la v5 ? une édition v5.1 basée sur sf2.8 ?)
Et poussé les gens à passer au moins en SPIP3.2, puis à passer en PHP7.4

N'ayant pas d'informations sur les extenstions PHP, la v5.1 doit être un phar web avec signature (algo à déterminer),
sans compression (au cas où même zlib n'est pas présent) et présenter un diagnostic complet sur les évolutions de plate-forme
à effectuer

le client http de cette version doit pouvoir fonctionne SANS `ext-curl`

PHP5.6
------

test avec docker php:5.6-cli

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer
ln -s /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
pear config-set php_ini /usr/local/etc/php/php.ini
pecl channel-update pecl.php.net
apt-get update && apt-get install -y git unzip libzip-dev
pecl install zip

config
pour les clients http implémentant les psr/design pattern suivants :



league/pipeline ^0.3.0
league/flysystem ^1.0 (ZipArchive adapter, SFTP, WebDav, ...)
league/container ^2.5
league/config N/A (^7.4 | ^8.0) donc pas utilisable pour la v5.1 donc pas retenu pour les v6-8
symfony/config ^3.4
symfony/console ^3.4
symfony/http-kernel ^3.4
symfony/finder peut-être pas utile si flysytem fait le job


Pour la v8
----------

outil de migration de base de données (avec up method, down method et seeders) probablement distinct
gestion des plugins
statistiques anonymisées sur spip.net (et fin du sites stats (plugin univers_spip))
nommer les stages ? rendre de pipeline observable (event-dispatcher ?)
isoler context et pipeline dans une lib distincte ?
publish-assets en postinstall ?
api "le plus securisée possible" dans spip pour obtenir les infos de plate-forme et d'instance

Idées archi remix
-----------------

-./composer.json
+./vendor/
+./sites/default/public/.htaccess
-./sites/default/public/index.php
-./sites/default/public/spip.php
-./sites/default/public/ecrire/index.php -> chdir ./ecrire/ ?
-./sites/default/public/IMG/
-./sites/default/public/local/
+./sites/default/public/app.js
+./sites/default/public/app.css
-./sites/default/tmp/
-./sites/default/config/connect.php
-./sites/default/config/mes_options.php
-./sites/default/squelettes/
-./prive/
-./ecrire/
-./plugins-dist/
-./squelettes-dist/
+./plugins/
+./config/
+./spip.php (en mode legacy) -> chdir ./sites/default/public/ ? ou cli à la artisan ?

Avec league/flysystem
---------------------

adapters souhaités :
local pour un install sur le serveur qui exécute le loader (accepte relatif, absolu et file:// et phar://)
inmemory pour tests unitaires et si possible le zip distant
ftp/sftp/ssh pour déployer une installation/mise à jour/mise à niveau sur un serveur distant (nécessite d'associer l'url ftp/ssh avec le host (url du site) de l'instance)

Pour le cli
require `league/flysystem-sftp` et `league/flysystem-ftp`
gérer `~/.netrc`, `~/.ssh/config`
gérer `~/.spip/config.json` -> array pour FtpConnectionOptions et SftpConnectionProvider

spip_installer
--------------

fournir DSN de connection à la base (ex: mysql://user:password@host:port/base)
fournir DSN ldap (ex: ldaps://user:password@host:port/o=spip.net,o=users)
fournir DSN smtp (ex: smtp://mailcatcher:1025)
fournir credentials du premier user si installation (email, password +nom+login)

fournir migration/seeders pour mettre à jour la base de données
mettre à jour les plugins
analyser squelettes... spipstan :)

spip_deployer
-------------
=spip_loader à distance

mode de détection de version d'instance spip installée
------------------------------------------------------

instance
    classic
        ecrire/inc_version.php (contient la version de spip)
        ecrire/paquet.xml (contient la version de spip)
        htaccess.txt (contient la version de spip)
        spip.php (default web router)
    mutualisation
        config/mes_options.php
        sites/xxx
        ecrire/inc_version.php (contient la version de spip)
        ecrire/paquet.xml (contient la version de spip)
        htaccess.txt (contient la version de spip)
        spip.php (default web router)
    remix
        sites/default/public/ecrire/index.php
        sites/default/public/spip.php
        vendor/spip/sdk/legacy/inc_version.php ?
        ecrire/paquet.xml ?
        version.php* ?
