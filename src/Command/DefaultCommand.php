<?php

namespace SpipRemix\Loader\Command;

use SpipRemix\Loader\Context;
use SpipRemix\Loader\Item\Instance;
use SpipRemix\Loader\Logger;
use SpipRemix\Loader\Pipeline;
use SpipRemix\Loader\Stage\CacheStage;
use SpipRemix\Loader\Stage\InitStage;
use SpipRemix\Loader\Stage\InstanceStage;
use SpipRemix\Loader\Stage\NetworkStage;
use SpipRemix\Loader\Stage\PlatformStage;
use SpipRemix\Loader\Stage\VersionSelectorStage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class DefaultCommand extends Command
{
    protected static $defaultName = 'spipremix:loader';

    protected function configure(): void
    {
        $this->setDescription('Install or Update a SPIP Instance');
        $this
            ->addArgument('instance', InputArgument::OPTIONAL, 'Instance directory [default: "' . getcwd() . '"]')
            ->addArgument('version', InputArgument::OPTIONAL, 'Prefered version to install')
            ->addOption(
                'channel',
                'c',
                InputOption::VALUE_REQUIRED,
                'Stability channel (stable, test or dev)',
                'stable'
            )
            ->addOption(
                'update-level',
                'u',
                InputOption::VALUE_REQUIRED,
                'Update level on stable channel (patch, minor or major)',
                'minor'
            )
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force installation if instance directory is a non empty and non SPIP directory'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = new Context();
        $logger = new Logger();
        $instance = strval($input->getArgument('instance'));
        if ($instance) {
            $context->set('instance', new Instance(directory: rtrim($instance, '/')));
        }
        $version = strval($input->getArgument('version'));
        if ($version) {
            $context->set('version', $version);
        }
        $channel = mb_strtolower(strval($input->getOption('channel')));
        if (!in_array($channel, ['dev', 'test', 'stable'])) {
            $output->writeln('<error>Wrong channel parameter</error>. (stable, test or dev)');

            return Command::INVALID;
        }
        $context->set('channel', $channel);

        $update = mb_strtolower(strval($input->getOption('update-level')));
        if (!in_array($update, ['patch', 'minor', 'major'])) {
            $output->writeln('<error>Wrong update parameter</error>. (patch, minor or major)');

            return Command::INVALID;
        }
        $context->set('update_level', $update);

        $context->set('force', $input->getOption('force'));

        $pipeline = new Pipeline([
            new InitStage(),
            new PlatformStage(),
            new NetworkStage(),
            new InstanceStage(),
            new CacheStage(),
            new VersionSelectorStage(),
        ]);

        $context = $pipeline($context, $logger);

        // Compter les versions installables possibles => adapter la suite du processus
        // return match (count($context->get('available_versions'))) {
        //     // 0 => arrêter le processus
        //     0 => $this->nothingTodo($output),
        //     // 1 => reprendre le process avec cette version
        //     1 => $this->install($output, $this->getOne($output, $context)),
        //     // 2+ => demander et reprendre le processus avec la réponse utilisateur
        //     default => $this->install($output, $this->askOne($input, $output, $context)),
        // };

        foreach ($logger->dump() as $log) {
            $decorator = '';
            if (in_array($log->level, ['warning'])) {
                $decorator = '<comment>';
            }
            if (in_array($log->level, ['error'])) {
                $decorator = '<error>';
            }
            $output->writeln($decorator . $log->message . '</>');
        }

        return Command::SUCCESS;
    }

    private function nothingTodo(OutputInterface $output): int
    {
        $output->writeln('<info>Aucune mise à jour.</info>');

        return Command::SUCCESS;
    }

    private function getOne(OutputInterface $output, Context $context): Context
    {
        $output->writeln('<info>Une seule mise à jour possible.</info>');
        $versions = (array) $context->get('available_versions');
        $context->set('branch_version', array_key_first($versions));

        return $context;
    }

    private function askOne(InputInterface $input, OutputInterface $output, Context $context): Context
    {
        $versions = (array) $context->get('available_versions');
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please select the version you want [default: "' . array_key_first($versions) . '"]',
            array_keys($versions),
            0
        );
        $question->setErrorMessage('Version %s is invalid.');
        $version = $helper->ask($input, $output, $question);
        $context->set('branch_version', $version);

        return $context;
    }

    private function install(OutputInterface $output, Context $context): int
    {
        $versions = (array) $context->get('available_versions');
        $version = $context->get('branch_version');
        $context->set('url_version', $versions[$version]['url']);
        if (isset($versions[$version]['sha1'])) {
            $context->set('sha1', $versions[$version]['sha1']);
        }

        // $pipe = new Pipe([
        //     new DownloadStep(),
        //     new CompareStep(),
        //     new ExtractStep(),
        //     new InstanceCleanupStep(),
        //     new CleanupCacheStep(),
        //     new CleanupLoaderStep(),
        //     new PostInstallStep(),
        // ]);

        // $context = $pipe($context, function ($context) {
        //     return $context;
        // });

        if (!empty($context->get('warnings'))) {
            foreach ($context->get('warnings') as $text) {
                $output->writeln('<comment>' . $text . '</comment>');
            }
        }
        if ($context->has('last_error')) {
            $output->writeln(sprintf('<error>%s</error>', $context->get('last_error')));

            return Command::FAILURE;
        }

        $instance = $context->get('instance');
        $output->writeln([
            sprintf('Instance directory: "%s"', $instance->directory),
            sprintf('Freespace: %s', $instance->freespace),
            sprintf('Zip Version: %s', $context->get('branch_version')),
            sprintf('Zip URL: "%s"', $context->get('url_version')),
        ]);
        if ($instance->getVersion()) {
            $output->writeln(sprintf('Previous installed SPIP Version: %s', $instance->getVersion()));
        }
        $output->writeln([
            sprintf('Taille nouveaux fichiers %d octets', $context->get('zip_size')),
            sprintf('Taille anciens fichiers %d octets', $instance->size),
        ]);

        return Command::SUCCESS;
    }
}
