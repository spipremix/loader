<?php

declare(strict_types=1);

namespace SpipRemix\Loader;

use SpipRemix\Loader\Exception\ItemNotFoundException;
use SpipRemix\Loader\Item\ItemInterface;

/**
 * Simple key–value data store.
 *
 * @author JamesRezo <james@rezo.net>
 */
class Context implements ContextInterface
{
    /** @var ItemInterface[] */
    private array $context = [];

    /**
     * Sets an entry.
     */
    public function set(string $id, ItemInterface $item): ContextInterface
    {
        $this->context[$id] = $item;

        return $this;
    }

    /**
     * Returns true if an entry name is defined.
     */
    public function has(string $id): bool
    {
        return array_key_exists($id, $this->context);
    }

    /**
     * Gets an entry.
     *
     * @throws ItemNotFoundException if the key is not defined
     */
    public function get(string $id): ItemInterface
    {
        if (!$this->has($id)) {
            throw new ItemNotFoundException(
                sprintf('id "%s" is not set in context.', $id)
            );
        }

        return $this->context[$id]; //->get();
    }

    /**
     * Removes an entry.
     */
    public function unset(string $id): self
    {
        if ($this->has($id)) {
            unset($this->context[$id]);
        }

        return $this;
    }

    /**
     * Clears all entries.
     */
    public function reset(): self
    {
        $this->context = [];

        return $this;
    }

    /**
     * Gets all entries.
     *
     * @return ItemInterface[]
     */
    public function all(): array
    {
        return $this->context;
    }
}
