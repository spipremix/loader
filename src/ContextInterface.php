<?php

namespace SpipRemix\Loader;

use Psr\Container\ContainerInterface;
use SpipRemix\Loader\Item\ItemInterface;

interface ContextInterface extends ContainerInterface
{
    public function set(string $id, ItemInterface $item): ContextInterface;
}
