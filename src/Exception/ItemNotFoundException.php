<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Exception;

use Exception;
use Psr\Container\NotFoundExceptionInterface;

class ItemNotFoundException extends Exception implements NotFoundExceptionInterface
{

}
