<?php

declare(strict_types=1);

namespace SpipRemix\Loader;

/**
 * Undocumented class.
 * @author JamesRezo <james@rezo.net>
 */
class Filesystem
{
    /**
     * Indicates if directory has been checked.
     */
    private bool $checked = false;

    /**
     * Filesystem contructor.
     *
     * @param string $directory
     * @param float  $freeSpace
     */
    public function __construct(
        protected string $directory,
        protected float $freeSpace = -1
    ) {
    }

    /**
     * Check a list of directories.
     *
     * ['ENV', 'subdir'] => ${ENV}/subdir
     *
     * @param non-empty-array<string[]> $checkDirs list of string couple
     * @return string|null the first existing directory that is writable
     */
    public static function checkDirs(array $checkDirs): ?string
    {
        $checkedDir = null;

        /** @var string[] $dirToCheck */
        foreach ($checkDirs as $dirToCheck) {
            $dir = getenv($dirToCheck[0]);
            if ($dir && is_dir($dir.$dirToCheck[1])) {
                $checkedDir = $dir.$dirToCheck[1];

                break;
            }
        }

        if ($checkedDir && !is_writable($checkedDir)) {
            throw new \InvalidArgumentException(sprintf('Directory "%s" is not a writable directory.', $checkedDir));
        }

        return $checkedDir;
    }

    /**
     * Undocumented function.
     *
     * @param string $file relative path to a file
     * @return string absolute path of the file in the filesystem directory
     */
    public function path(string $file = ''): string
    {
        if (!$this->checked) {
            $directory = realpath(rtrim($this->directory, '/'));
            if (false === $directory || !is_writable($directory)) {
                throw new \InvalidArgumentException(sprintf('Directory "%s" is not a writable directory.', $this->directory));
            }
            $this->directory = $directory;
            $this->checked = true;
        }

        return $this->directory.'/'.trim($file, '/');
    }

    /**
     * Undocumented function.
     */
    public function freespace(): float
    {
        if (-1.0 === $this->freeSpace) {
            $freeSpace = getenv('SPIP_FAKE_FREE_SPACE');
            if ($freeSpace) {
                if ($freeSpace != (float) $freeSpace) {
                    throw new \InvalidArgumentException('SPIP_FAKE_FREE_SPACE is not correctly set. Should be a value in bytes.');
                }
                $this->freeSpace = (float) $freeSpace;
            } else {
                $this->freeSpace = (float) disk_free_space($this->path());
            }
        }

        return $this->freeSpace;
    }
}
