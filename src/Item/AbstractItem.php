<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Item;

abstract class AbstractItem implements ItemInterface
{
    /**
     * {@inheritDoc}
     */
    abstract public static function init(): self;

    /**
     * {@inheritDoc}
     */
    abstract public function with(...$parameters): self;
}
