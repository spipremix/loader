<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Item;

class Api extends AbstractItem implements ItemInterface
{
    public static function init(): self
    {
        return new self();
    }

    public function with(...$parameters): self
    {
        return $this;
    }
}
