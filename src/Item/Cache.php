<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Item;

use SpipRemix\Loader\Filesystem;

class Cache implements ItemInterface
{
    private const DEFAULT_CACHE_PATH = [
        ['SPIP_CACHE_DIRECTORY', ''],
        ['XDG_CONFIG_HOME', '/spip'],
        ['HOME', '/.spip'],
    ];

    protected function __construct(
        private bool $cleanable = false,
        protected ?Filesystem $filesystem = null,
    ) {
    }

    public static function init(): self
    {
        $filesystem = null;
        $directory = Filesystem::checkDirs(static::DEFAULT_CACHE_PATH);
        if (!is_null($directory)) {
            $filesystem = new Filesystem($directory);
        }

        return new self(filesystem: $filesystem);
    }

    public function with(...$parameters): self
    {
        return $this;
    }

    public function isCleanable(): bool
    {
        return $this->cleanable;
    }

    public function setCleanable(bool $bool): self
    {
        $this->cleanable = $bool;

        return $this;
    }

    public function path(string $file = ''): string
    {
        return $this->filesystem->path($file);
    }

    public function freespace(): float
    {
        return $this->filesystem->freespace();
    }
}
