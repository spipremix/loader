<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Item;

use SpipRemix\Loader\Filesystem;

class Instance implements ItemInterface
{
    protected ?bool $empty = null;

    protected ?string $version = null;

    protected function __construct(
        protected ?Filesystem $filesystem = null,
    ) {
    }

    public static function init(): self
    {
        return new self();
    }

    public function with(...$parameters): self
    {
        return $this;
    }

    public function isEmpty(): bool
    {
        if (is_null($this->empty)) {
            $scan = scandir($this->path()) ?: ['.', '..', basename($_SERVER['SCRIPT_FILENAME'])];
            $scan = array_diff($scan, ['.', '..', basename($_SERVER['SCRIPT_FILENAME'])]);
            $this->empty = 0 === count($scan);
        }

        return $this->empty;
    }

    public function getVersion(): ?string
    {
        if (is_null($this->version)) {
            $this->version = $this->getInstalledVersion();
        }

        return $this->version;
    }

    protected function getInstalledVersion(): ?string
    {
        if (!is_readable($this->path('ecrire/inc_version.php'))) {
            // throw new \Exception('ecrire/inc_version.php not found. Seems instance is not SPIP.');
            return null;
        }

        $finfo = finfo_open(FILEINFO_MIME);
        if ($finfo) {
            $fileMimeType = (string) finfo_file($finfo, $this->path('ecrire/inc_version.php')) ?: '';
            finfo_close($finfo);
        } else {
            throw new \RuntimeException('Cannot detect mime-type of ecrire/inc_version.php in instance');
        }
        if (!preg_match('/^text\/x-php;/', $fileMimeType)) {
            throw new \Exception('wrong mime type for ecrire/inc_version.php');
        }

        $lines = preg_grep(
            '/^\$spip_version_branche\s*=.*;$/',
            preg_split(
                '/[\r\n]/',
                file_get_contents($this->path('ecrire/inc_version.php')) ?: ''
            ) ?: []
        ) ?: [];
        $line = array_pop($lines);
        if (empty($line)) {
            throw new \Exception('wrong ecrire/inc_version.php');
        }

        $spip_version_branche = '';
        eval($line);
        $installedVersion = mb_substr($spip_version_branche, 0, 3);
        if (!preg_match('/^\d+\.\d+$/', $installedVersion)) {
            throw new \Exception('wrong spip_version_branche');
        }

        $this->version = $spip_version_branche;

        return $this->version;
    }

    public function path(string $file = ''): string
    {
        return $this->filesystem->path($file);
    }

    public function freespace(): float
    {
        return $this->filesystem->freespace();
    }
}
