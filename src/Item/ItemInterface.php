<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Item;

interface ItemInterface
{
    /**
     * Gives an empty Item.
     *
     * @return self
     */
    public static function init(): self;

    /**
     * Gives parameters to the Item.
     *
     * @param string ...$parameters
     * @return self
     */
    public function with(...$parameters): self;
}
