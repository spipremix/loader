<?php

namespace SpipRemix\Loader\Item;

class Parameter implements ItemInterface
{
    protected function __construct()
    {

    }

    public static function init(): ItemInterface
    {
        return new self();
    }

    public function with(...$parameters): ItemInterface
    {
        return $this;
    }
}
