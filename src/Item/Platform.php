<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Item;

class Platform extends AbstractItem implements ItemInterface
{
    protected function __construct(
        private string $version,
        private int $memoryLimit,
        /** @var string[] $extensions */
        private array $extensions
    ) {
    }

    public static function init(): self
    {
        $version = getenv('SPIP_FAKE_PLATFORM_VERSION') ?: mb_substr(PHP_VERSION, 0, 3);
        $memoryLimit = self::getActualMemoryLimit();
        if ($extensions = getenv('SPIP_FAKE_PLATFORM_EXTENSIONS')) {
            $extensions = explode(',', (string) $extensions);
        } else {
            $extensions = get_loaded_extensions();
        }

        return new self(version: $version, memoryLimit: $memoryLimit, extensions: $extensions);
    }

    public function with(...$parameters): self
    {
        return $this;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getMemoryLimit(): int
    {
        return $this->memoryLimit;
    }

    /** @return string[] */
    public function getExtensions(): array
    {
        return $this->extensions;
    }

    private static function getActualMemoryLimit(): int
    {
        $actualMemoryLimit = (string) (getenv('SPIP_FAKE_PLATFORM_MEMORY_LIMIT') ?: ini_get('memory_limit'));
        sscanf($actualMemoryLimit, '%u%c', $number, $suffix);
        if (isset($suffix)) {
            $exponent = (int) strpos(' KMG', strtoupper($suffix));
            $number = $number * pow(1024, $exponent);
        }

        return $number;
    }
}
