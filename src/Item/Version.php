<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Item;

/**
 * Version Factory.
 *
 * @author JamesRezo <james@rezo.net>
 */
class Version extends AbstractItem implements ItemInterface
{
    public static function init(): self
    {
        return new self();
    }

    public function with(...$parameters): self
    {
        return $this;
    }
}
