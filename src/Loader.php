<?php

namespace SpipRemix\Loader;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\Pipeline;

/**
 * Micro-Kernel.
 *
 * @author JamesRezo <james@rezo.net>
 */
class Loader
{
    public function __construct(private array $stages = [], private ContextInterface $context, private LoggerInterface $logger)
    {

    }

    public function getPipeline(): Pipeline
    {
        return new Pipeline($this->stages);
    }
}
