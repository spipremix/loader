<?php

namespace SpipRemix\Loader;

use Psr\Log\AbstractLogger;
use SpipRemix\Loader\Logger\Record;

class Logger extends AbstractLogger
{
    /** @var Record[] */
    private array $logs = [];

    /**
     * {@inheritDoc}
     *
     * @param string       $level
     * @param array<mixed> $context
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        if (in_array($level, ['emergency', 'alert', 'critical'])) {
            throw new \Exception($message);
        }

        $this->logs[] = new Record(level: $level, message: $message, context: $context);
    }

    /** @return \Generator<int, Record> */
    public function dump(): \Generator
    {
        foreach ($this->logs as $log) {
            yield $log;
        }
    }
}
