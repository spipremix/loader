<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Logger;

class Record
{
    /**
     * Entry for the Logger.
     *
     * @param string             $level
     * @param string|\Stringable $message
     * @param array<mixed>       $context
     */
    public function __construct(public string $level, public string|\Stringable $message, public array $context = [])
    {
    }

    public function __toString()
    {
        return mb_strtoupper($this->level).': '.$this->message;
    }
}
