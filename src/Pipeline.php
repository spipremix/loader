<?php

declare(strict_types=1);

namespace SpipRemix\Loader;

use Psr\Log\LoggerInterface;

/**
 * Pipeline pattern implementation.
 *
 * @author JamesRezo <james@rezo.net>
 */
class Pipeline implements StageInterface
{
    /** @param StageInterface[] $stages */
    public function __construct(private array $stages = [])
    {
    }

    /**
     * {@inheritDoc}
     */
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        try {
            $context = array_reduce(
                $this->stages,
                function (ContextInterface $context, StageInterface $stage) use ($logger) {
                    return $stage($context, $logger);
                },
                $context
            );
        } catch (\Throwable $th) {
            $logger->error($th->getMessage());
        }

        return $context;
    }
}
