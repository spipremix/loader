<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\Item\Cache;
use SpipRemix\Loader\StageInterface;

class CacheStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        // $logger->debug('>cache stage');

        if (!$context->has('instance')) {
            throw new \Exception('Please, run Init stage before Cache stage.');
        }

        /** @var \SpipRemix\Loader\Item\Instance */
        $instance = $context->get('instance');

        /** @var Cache */
        $cache = Cache::init();
        if (!$cache->path()) {
            $cache->with($instance->path())->setCleanable(true);
        }
        $context->set('cache', $cache);
        // $logger->info(sprintf('cache is located at "%s"', $context->get('cache')->path()));
        // $logger->info(sprintf('cache freespace is %d', $context->get('cache')->freespace()));

        return $context;
    }
}
