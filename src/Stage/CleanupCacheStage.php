<?php

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\Item\Cache;
use SpipRemix\Loader\StageInterface;

class CleanupCacheStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        /** @var Cache */
        $cache = $context->get('cache');
        if ($cache->isCleanable()) {
            unlink($cache->path(basename(strval($context->get('url_version')))));
        }

        return $context;
    }
}
