<?php

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\Item\Instance;
use SpipRemix\Loader\StageInterface;

class CleanupInstanceStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        /** @var Instance $instance */
        $instance = $context->get('instance');
        // foreach ($instance->filesToDelete() as $file) {
        //     unlink($file);
        // }
        // $instance->filesToDelete = [];

        return $context;
    }
}
