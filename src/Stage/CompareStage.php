<?php

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\StageInterface;
use Symfony\Component\Finder\Finder;

class CompareStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        $toDir = $context->get('instance')->directory;
        $fromZip = $context->get('cache_directory').'/'.basename($context->get('url_version'));
        $regexp = $this->regexpFilter();
        $filterInstalled = function (\SplFileInfo $file) use ($toDir, $regexp) {
            /** @var string */
            $installedFile = str_replace($toDir.'/', '', $file);

            return (bool) preg_match($regexp, $installedFile);
        };

        $uncompressedSize = 0;
        $finder = new Finder();
        $finder->files()->in('phar://'.$fromZip)->ignoreDotFiles(false)->exclude(['IMG', 'local', 'tmp']);
        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($finder as $file) {
            $uncompressedSize += $file->getSize();
        }
        $filesFromZip = iterator_to_array($finder);

        $installedSize = 0;
        $finder = new Finder();
        $finder->files()->in($toDir)->filter($filterInstalled);
        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($finder as $file) {
            $installedSize += $file->getSize();
        }
        $filesFromInstallationDirectory = iterator_to_array($finder);

        // @todo $context->set('zip_size', $uncompressedSize);
        $instance = $context->get('instance');
        $instance->size = $installedSize;
        $toDelete = array_diff(array_map(function ($file) {
            return $file->getRelativePathname();
        }, $filesFromInstallationDirectory), array_map(function ($file) {
            return $file->getRelativePathname();
        }, $filesFromZip));
        if (!empty($toDelete)) {
            $instance->filesToDelete = $toDelete;
        }
        $context->set('instance', $instance);

        // @todo Check freespace

        return $context;
    }

    private function regexpFilter(): string
    {
        return '/^('.join('|', array_map(function ($pattern) {
            return preg_quote($pattern, '/');
        }, [
            'config/ecran_securite.php',
            'ecrire/',
            'plugins-dist/',
            'prive/',
            'squelettes-dist/',
            '.gitignore',
            'CHANGELOG.txt',
            'COPYING.txt',
            'INSTALL.txt',
            'SECURITY.md',
            'htaccess.txt',
            'index.php',
            'plugins-dist.json',
            'spip.php',
            'spip.png',
            'spip.svg',
        ])).')/';
    }
}
