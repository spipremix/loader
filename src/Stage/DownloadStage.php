<?php

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\StageInterface;

class DownloadStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        $zipFile = $context->get('cache_directory').'/'.basename($context->get('url_version'));
        if (!file_exists($zipFile)) {
            $response = $context->get('http_client')->request('HEAD', $context->get('url_version'))->getHeaders();
            $length = $response['content-length'][0];
            if ($length > $context->get('cache_freespace')) {
                throw new \Exception('Not enough space to download SPIP '.$context->get('branch_version'));
            }
            $zip = $context->get('http_client')->request('GET', $context->get('url_version'));
            $fileHandler = fopen($zipFile, 'w');
            if (!$fileHandler) {
                throw new \Exception('Cannot create "'.$zipFile.'" file.');
            }
            foreach ($context->get('http_client')->stream($zip) as $chunk) {
                fwrite($fileHandler, $chunk->getContent());
            }
            fclose($fileHandler);

            if ($context->has('sha1')) {
                $sha1Zip = sha1_file($zipFile);
                if ($context->get('sha1') !== $sha1Zip) {
                    // Remove the wrong downloaded zip file anywhere it is
                    unlink($zipFile);

                    throw new \Exception('sha1 control failed');
                }
            }
        }

        return $context;
    }
}
