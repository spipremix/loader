<?php

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\StageInterface;
use Symfony\Component\Finder\Finder;

class ExtractStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        $toDir = $context->get('instance')->directory;
        $fromZip = $context->get('cache_directory').'/'.basename($context->get('url_version'));

        $finder = new Finder();
        $finder->files()->in('phar://'.$fromZip)->ignoreDotFiles(false)->exclude(['IMG', 'local', 'tmp']);
        $filesFromZip = iterator_to_array($finder);

        // The real extract
        $zip = new \ZipArchive();
        $zip->open($fromZip);
        $zip->extractTo($toDir, array_values(array_map(function ($file) {
            return $file->getRelativePathname();
        }, $filesFromZip)));

        foreach (['IMG', 'local', 'tmp'] as $dir) {
            if (!is_dir($toDir.'/'.$dir)) {
                mkdir($toDir.'/'.$dir);
            }
        }

        return $context;
    }
}
