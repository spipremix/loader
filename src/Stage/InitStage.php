<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\Item\Instance;
use SpipRemix\Loader\StageInterface;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Undocumented class.
 */
class InitStage implements StageInterface
{
    /**
     * {@inheritDoc}
     */
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        // $logger->debug('>init stage');

        if (!$context->has('instance')) {
            $instance = Instance::init()->with(/**[
                ['SPIP_INSTANCE_DIRECTORY', ''],
            ]*/); // ->path() ?? dirname($_SERVER['SCRIPT_FILENAME']);

            $context->set('instance', $instance);
        }

        // if (!$context->has('channel')) {
        //     $context->set('channel', (string) (getenv('SPIP_LOADER_CHANNEL') ?: 'stable'));
        // }

        // if (!$context->has('update_level')) {
        //     $context->set('update_level', (string) (getenv('SPIP_LOADER_UPDATE_LEVEL') ?: 'minor'));
        // }

        // if (!$context->has('force')) {
        //     $context->set('force', false);
        // }

        // $context
        //     ->set('self_memory_limit', 33554432)
        //     ->set('http_client', HttpClient::create())
        //     ->set('api_url', (string) (getenv('SPIP_API_URL') ?: 'https://www.spip.net/spip_loader.api/3'))
        //     ->set('cleanup_cache', true)
        // ;

        return $context;
    }
}
