<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\StageInterface;

class InstanceStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        // $logger->debug('>instance stage');

        if (!$context->has('instance')) {
            throw new \Exception('No Instance Directory.');
        }

        /** @var \SpipRemix\Loader\Item\Instance */
        $instance = $context->get('instance');

        $logger->info(sprintf('instance is located at "%s"', $instance->path()));
        $logger->info(sprintf('instance freespace is %d', $instance->freespace()));
        if ($instance->isEmpty()) {
            $logger->info('Nous allons procéder à une installation de SPIP.');
        } elseif ($version = $instance->getVersion()) {
            $logger->info(sprintf('La version actuellement installée est %s.', $version));
            $logger->info('Nous allons procéder à une mise à jour ou à une mise à niveau.');
        } else {
            $logger->warning('Attention, l\'instance ne semble pas être un SPIP.');
            if ($context->get('force')) {
                $logger->info('Nous allons procéder à une mise à jour ou à une mise à niveau malgré tout. (force=true)');
            } else {
                throw new \Exception(sprintf('Veuillez vérifier le contenu du répertoire "%s".', $instance->path()));
            }
        }

        return $context;
    }
}
