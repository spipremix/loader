<?php

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\StageInterface;

class InteractiveStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        if ($context->has('question')) {
            $answer = $context->get('question')->ask();
            if ($this->isValid($answer)) {
                $context = $context->set('answer', $answer);
            }
        }

        return $context;
    }

    private function isValid(mixed $answer): bool
    {
        return true;
    }
}
