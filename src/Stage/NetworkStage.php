<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\StageInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NetworkStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        // $logger->debug('>network stage');

        if (!$context->has('api_url')) {
            throw new \Exception('No API URL');
        }
        if (!$context->has('http_client')) {
            throw new \Exception('No HTTP Client');
        }
        $client = $context->get('http_client');
        if (!($client instanceof HttpClientInterface)) {
            throw new \Exception('Wrong HTTTP Client');
        }
        $api = $client->request('GET', $context->get('api_url'), ['timeout' => 1.0])->toArray();
        if (!isset($api['versions']['dev']['url'])) {
            throw new \Exception('Incomplete API.');
        }
        $client->request('HEAD', $api['versions']['dev']['url'], ['timeout' => 1.0]);
        // @todo $context->set('api', $api);

        // $logger->info(sprintf('API version is %d', $context->get('api')['api']));

        return $context;
    }
}
