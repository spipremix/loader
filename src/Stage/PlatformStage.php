<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\Item\Platform;
use SpipRemix\Loader\StageInterface;

class PlatformStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        // $logger->debug('>platform stage');

        $context->set('platform', Platform::init());
        // $logger->info(sprintf('PHP version is %s', $context->get('platform')->getVersion()));
        if ($context->get('platform')->getMemoryLimit() < $context->get('self_memory_limit')) {
            throw new \Exception('This loader cannot run with memory_limit<32M.');
        }

        // ext-mbstring, ext-zip required for this actual spipremix_loader
        $diff = array_diff(['zip', 'mbstring'], $context->get('platform')->getExtensions());
        if (!empty($diff)) {
            throw new \Exception(implode(',', $diff).' extension is required to run this loader.');
        }

        // ext-curl for this actual spipremix_loader is recommanded for performance
        $diff = array_diff(['curl'], $context->get('platform')->getExtensions());
        if (!empty($diff)) {
            $logger->warning(implode(',', $diff).' extension is recommanded to run this loader.');
        }

        return $context;
    }
}
