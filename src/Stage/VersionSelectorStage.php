<?php

declare(strict_types=1);

namespace SpipRemix\Loader\Stage;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\Item\Instance;
use SpipRemix\Loader\StageInterface;

class VersionSelectorStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        // $logger->debug('>version stage');

        $channels = [
            'dev' => ',^dev$,',
            'test' => ',-(alpha|beta|RC)\d*$,',
            'stable' => ',^\d+\.\d+\.\d+$,',
        ];
        $regexpChannel = $channels[$context->get('channel')];

        $versions = array_filter(
            $context->get('api')['versions'],
            function ($requirements, $version) use ($context, $regexpChannel) {
                return
                    // SPIP version matches the channel
                    (bool) preg_match($regexpChannel, $version)
                    // php matches the required versions
                    && in_array($context->get('platform')->getVersion(), $requirements['php'])
                    // memory_limit matches the minimum required
                    && $context->get('platform')->getMemoryLimit() >= ($requirements['ram'] ?? 0)
                    // extensions are matching
                    && $this->checkExtensions($requirements['extensions'] ?? [])
                    // version matches installed version and update level flag
                    && $this->checkVersusInstalledVersion($version, $context)
                ;
            },
            ARRAY_FILTER_USE_BOTH
        );

        if ($context->get('instance')->isEmpty() && count($versions) > 1) {
            $version = array_key_first($versions);
            $versions = [$version => $versions[$version]];
        }

        if ($context->has('version')) {
            $askedVersion = $context->get('version');
            $versions = array_filter(
                $versions,
                function ($version) use ($askedVersion) {
                    return $version === $askedVersion;
                },
                ARRAY_FILTER_USE_KEY
            );
        }

        // @todo $context->set('available_versions', $versions);

        if (!empty($v = array_keys($versions))) {
            $logger->info(sprintf('Loader will install %s version', implode(' or ', $v)));
        }

        return $context;
    }

    /**
     * Check recursively PHP extensions.
     *
     * @param array<mixed> $extensions
     */
    private function checkExtensions(array $extensions, string $logicalOperation = 'and'): bool
    {
        $tmpExtensions = true;

        foreach ($extensions as $extension => $version) {
            if (is_array($version)) {
                if ('or' === $logicalOperation) {
                    $tmpExtensions |= $this->checkExtensions($version, $extension);
                } else {
                    $tmpExtensions &= $this->checkExtensions($version, $extension);
                }
            } else {
                $tmpExtensions &= extension_loaded($extension) && ('*' === $version || phpversion($extension) >= $version);
            }

            // if (!$tmpExtensions) {
            // @todo Register info about missing/wrong version extension if $tmpExtensions fall to false
            // "$extension is missing (if != '*' then or wrong version (at least $version needed)"
            // if $extension is 'or' ...
            // }
        }

        return (bool) $tmpExtensions;
    }

    private function checkVersusInstalledVersion(string $version, ContextInterface $context): bool
    {
        $channel = $context->get('channel');
        if ('stable' !== $channel) {
            return true;
        }

        /** @var Instance $instance */
        $instance = $context->get('instance');
        if ($instance->getVersion()) {
            $updateLevel = $context->get('update_level');

            return match ($updateLevel) {
                'patch' => $this->versionCompare('\d+\.\d+', $version, $instance->getVersion()),
                default /*'minor'*/ => $this->versionCompare('\d+', $version, $instance->getVersion()),
                'major' => 1 === version_compare($version, $instance->getVersion()),
            };
        }

        return true;
    }

    private function versionCompare(string $mode, string $version, string $installed): bool
    {
        $modeV = preg_replace(',^('.$mode.')\..*,', '$1', $version);
        $modeI = preg_replace(',^('.$mode.')\..*,', '$1', $installed);
        $version = preg_replace(',^'.$mode.'\.,', '', $version) ?? '';
        $installed = preg_replace(',^'.$mode.'\.,', '', $installed) ?? '';

        return $modeV == $modeI && 1 === version_compare($version, $installed);
    }
}
