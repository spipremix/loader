<?php

namespace SpipRemix\Loader;

use Psr\Log\LoggerInterface;

/**
 * Pipeline interface.
 *
 * @author JamesRezo <james@rezo.net>
 */
interface StageInterface
{
    /**
     * Callable.
     */
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface;
}
