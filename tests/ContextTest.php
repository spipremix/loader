<?php

namespace SpipRemix\Loader\Tests;

use SpipRemix\Loader\Context;
use SpipRemix\Loader\Exception\ItemNotFoundException;
use PHPUnit\Framework\TestCase;
use SpipRemix\Loader\Tests\Fixtures\DummyItem;

/**
 * @covers SpipRemix\Loader\Context
 * @author JamesRezo <james@rezo.net>
 */
class ContextTest extends TestCase
{
    private Context $context;

    protected function getStubItem(int $integer)
    {
        return DummyItem::init()->with($integer);
    }

    protected function setUp(): void
    {
        $this->context = new Context();
    }

    protected function fill(): void
    {
        $this->context->set('test1', $this->getStubItem(1));
        $this->context->set('test2', $this->getStubItem(2));
    }

    public function testGetSet()
    {
        $this->context->set('test', $this->getStubItem(0));
        /** @var DummyItem */
        $actual = $this->context->get('test');

        $this->assertEquals(0, $actual->get());

        try {
            $this->context->get('notset');
            $this->fail('->get() throws a LogicException if the id does not exist');
        } catch (ItemNotFoundException $e) {
            $this->assertInstanceOf(ItemNotFoundException::class, $e, '->get() throws an ItemNotFoundException if the id does not exist');
            $this->assertEquals('id "notset" is not set in context.', $e->getMessage(), '->get() throws an ItemNotFoundException if the id does not exist');
        }
    }

    public function testUnset()
    {
        $this->fill();
        $this->context->unset('test1');
        $this->assertEquals(['test2' => DummyItem::init()->with(2)], $this->context->all());
    }

    public function testReset()
    {
        $this->fill();
        $this->context->reset();
        $this->assertEquals([], $this->context->all());
    }
}
