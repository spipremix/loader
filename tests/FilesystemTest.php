<?php

namespace SpipRemix\Loader\Tests;

use PHPUnit\Framework\TestCase;
use SpipRemix\Loader\Filesystem;

/**
 * @covers SpipRemix\Loader\Filesystem
 * @author JamesRezo <james@rezo.net>
 */
class FilesystemTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        mkdir('/tmp/not-writable', 0500);
    }

    public static function tearDownAfterClass(): void
    {
        chmod('/tmp/not-writable', 0700);
        rmdir('/tmp/not-writable');
    }

    public function dataWrongPath()
    {
        return [
            'not-exist' => [
                'Directory "/not-exist" is not a writable directory.',
                '/not-exist',
            ],
            // 'not-writable' => [
            //     'Directory "/tmp/not-writable" is not a writable directory.',
            //     '/tmp/not-writable',
            // ],
        ];
    }

    /**
     * @dataProvider dataWrongPath
     */
    public function testWrongPath($expected, $directory)
    {
        // Given
        $filesystem = new Filesystem($directory);

        // Then
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        // When
        $filesystem->path();
    }

    public function testPath()
    {
        // Given
        $filesystem = new Filesystem('/tmp');
        $filesystem->path(); # checked => true

        // When
        $actual = $filesystem->path('test');

        // Then
        $this->assertEquals('/tmp/test', $actual);
    }

    public function testFreespace()
    {
        // Given
        $filesystem = new Filesystem('/tmp');

        // When
        $actual = $filesystem->freespace();

        // Then
        $this->assertEquals(100, $actual);
    }

    public function testCheckDirs($expected, $dirs)
    {
        // Given
        $dirs = ['XDG_CONFIG_HOME', '/spip',];

        // When
        $actual = Filesystem::checkDirs($dirs);

        // Then
        $this->assertNull($actual);
    }

    public function testWrongCheckDirs()
    {
        // Then
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Directory "/tmp/not-writable" is not a writable directory.');

        // When
        Filesystem::checkDirs([['SPIP_CACHE_DIRECTORY', ''],]);
    }
}
