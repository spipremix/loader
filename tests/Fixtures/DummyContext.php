<?php

namespace SpipRemix\Loader\Tests\Fixtures;

use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\Item\ItemInterface;

class DummyContext implements ContextInterface
{
    public function get(string $id): mixed
    {
        return DummyItem::init();
    }

    public function has(string $id): bool
    {
        return true;
    }

    public function set(string $id, ItemInterface $item): ContextInterface
    {
        return $this;
    }
}
