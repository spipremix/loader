<?php

namespace SpipRemix\Loader\Tests\Fixtures;

use SpipRemix\Loader\Item\ItemInterface;

class DummyItem implements ItemInterface
{
    protected function __construct(private int $dummy)
    {
    }

    public static function init(): self
    {
        return new self(dummy: 0);
    }

    /** @param string $parameters */
    public function with(...$parameters): self
    {
        $this->dummy = $parameters[0];
        return $this;
    }

    public function get(): int
    {
        return $this->dummy;
    }
}
