<?php

namespace SpipRemix\Loader\Tests\Fixtures;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class DummyLogger extends NullLogger implements LoggerInterface
{
    public string $error;

    public function log($level, string|\Stringable $message, array $context = []): void
    {
        if ('error' === $level) {
            $this->error = 'ERROR: '.$message;
        }
    }
}
