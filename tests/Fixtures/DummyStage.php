<?php

namespace SpipRemix\Loader\Tests\Fixtures;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\StageInterface;
use SpipRemix\Loader\Tests\Fixtures\DummyItem;

class DummyStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        return $context->set('dummy', DummyItem::init());
    }
}
