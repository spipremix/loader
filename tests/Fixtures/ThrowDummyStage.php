<?php

namespace SpipRemix\Loader\Tests\Fixtures;

use Psr\Log\LoggerInterface;
use SpipRemix\Loader\ContextInterface;
use SpipRemix\Loader\StageInterface;

class ThrowDummyStage implements StageInterface
{
    public function __invoke(ContextInterface $context, LoggerInterface $logger): ContextInterface
    {
        throw new \Exception('Error Processing Request');

        return $context;
    }
}
