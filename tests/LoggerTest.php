<?php

namespace SpipRemix\Loader\Tests;

use Exception;
use PHPUnit\Framework\TestCase;
use SpipRemix\Loader\Logger;

/**
 * @covers SpipRemix\Loader\Logger
 * @covers SpipRemix\Loader\Logger\Record
 * @author JamesRezo <james@rezo.net>
 */
class LoggerTest extends TestCase
{
    private Logger $logger;

    protected function setUp(): void
    {
        $this->logger = new Logger();
    }

    public function testInfoLevels()
    {
        // Given
        $message = 'test';

        // When
        $this->logger->debug($message);

        // Then
        $actual = '';
        foreach ($this->logger->dump() as $record) {
            $actual .= $record;
        }
        $this->assertEquals('DEBUG: test', $actual);
    }

    public function testErrorLevels()
    {
        // Given
        $message = 'test';

        // When
        try {
            $this->logger->emergency($message);
            $this->fail('test');
        } catch (Exception $e) {
            // Then
            $this->assertInstanceOf(Exception::class, $e);
            $this->assertEquals('test', $e->getMessage());
        }
    }
}
