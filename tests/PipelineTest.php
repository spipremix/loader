<?php

namespace SpipRemix\Loader\Tests;

use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use SpipRemix\Loader\Pipeline;
use SpipRemix\Loader\Tests\Fixtures\DummyContext;
use SpipRemix\Loader\Tests\Fixtures\DummyLogger;
use SpipRemix\Loader\Tests\Fixtures\DummyStage;
use SpipRemix\Loader\Tests\Fixtures\ThrowDummyStage;

/**
 * @covers SpipRemix\Loader\Pipeline
 * @author JamesRezo <james@rezo.net>
 */
class PipelineTest extends TestCase
{
    public function testEmptyStages()
    {
        // Given
        $expected = new DummyContext();
        $pipeline = new Pipeline();

        // When
        $actual = $pipeline(new DummyContext(), new NullLogger());

        // Then
        $this->assertEquals($expected, $actual);
    }

    public function testProcessDummyStage()
    {
        // Given
        $pipeline = new Pipeline([new DummyStage()]);

        // When
        $context = $pipeline(new DummyContext(), new NullLogger());
        /** @var SpipRemix\Loader\Tests\Fixtures\DummyItem */
        $actual = $context->get('dummy');

        // Then
        $this->assertEquals(0, $actual->get());
    }

    public function testProccesErrors()
    {
        // Given
        $logger = new DummyLogger();
        $pipeline = new Pipeline([new ThrowDummyStage()]);

        // When
        $pipeline(new DummyContext(), $logger);

        // Then
        $this->assertEquals('ERROR: Error Processing Request', $logger->error);
    }
}
